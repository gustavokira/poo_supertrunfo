# Super Trunfo
Implementar uma das possíveis formas do jogo Super Trunfo. Mais info em: https://pt.wikipedia.org/wiki/Super_Trunfo.
Objetivo Geral: Trabalhar com um exercício concreto de complexidade média. 
As e os dicentes devem trabalhar com a capacidade de reconhecer um problema e transpô-lo para
um modelo oriendado a objetos usando os conceitos vistos até então.

1. Um modo de jogo somente: Um Humano contra um computador.

2. Pode ser feito em até três pessoas.

3. Enviar os códigos para o email do professor.

### Primeira fase (entrega: 28 de Março - QUINTA FEIRA): 
Objetivo: entender o problema a ser implementado.

1. Jogar o jogo e entender bem as regras.

2. Descrever com frases (texto escrito) cada etapa do jogo. 
	- Ex: Embaralhar as cartas em um monte, distribuir cartas para os jogadores. etc...
	
	
3. Enumerar todas as ações de jogadores possíveis dentro do jogo.

4. Criar classes com atributos e métodos que dêem conta das ações enumeradas (não precisa ser código, por hora).
	- Ex: jogador compra carta:
		Classe jogador tem um método receberCarta que tem como atributo um objeto Carta.
		Classe Baralho tem um método retirarCarta que retorna um objeto Carta.
	
5. Descrever cada caso com as chamadas de métodos necessárias para executar um caso.

6. Tentar separar entrada de dados do funcionamento do jogo. (opcional)

7. O computador deve jogar de forma automática. Qualidade do desempenho não irá diminuir a nota do trabalho, mas pode vir a contar como pontos.

### Segunda fase (entrega: 11 de Abril):
Objetivo: Transpor o problema para o computador na forma de código orientado a objetos.

1. Escrever o código de todas as classes.

2. Implementar as interações entre as classes.

### Avalição
A nota será composta pela soma dos três primeiros itens. O quarto item é um modificador. Qualquer integrante do gruopo pode ser 
avaliado individualmente e caso não apresente respostas satisfatórias, sua nota pode ser mudada.

1. (0.33) Uso dos conceitos de orientação a objetos: Classes, objetos, atributos, valores e métodos.

2. (0.33) Robustez do código construido:
	- O quão fácil é adicionar ou retirar uma carta do jogo?
	- O quão fácil é alterar a quantidade de cartas iniciais de cada jogador?
	- O quão fácil é executar o jogo mais de uma vez?
	- O quão fácil é adicionar outros modos de jogo? (3 jogadores, 1 jogador e 2 máquinas...)


3. (0.33) Funcionamento correto.
	- O jogo acaba em todos os momentos corretos?
	- O jogador consegue executar todas as ações que têm direito?


4. Domínio do integrante da equipe daquilo que foi feito.
	- Sabe para que serve cada classe?
	- Sabe porque cada método está em um classe e não na outra?
	- Se trocar algum trecho de código de lugar, consegue corrigir o modelo?
	- Consegue entender como estão representados os conceitos de orientação a objetos no código?

