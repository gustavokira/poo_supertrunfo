import java.util.ArrayList;
import java.util.Random;

class Main {
  public static void main(String[] args) {
    JogoPedraMaior jcm = new JogoPedraMaior();
    jcm.iniciar();
    for(int i =0;i<10;i++){
      jcm.rodada();
    }

  }
}

class Pedra{
  int valor;
}

class JogoPedraMaior{

  Jogador jogadorA;
  Jogador jogadorB;
  ArrayList<Pedra> monte;

  Jogador primeiroJogadorDaRodada;
  Jogador segundoJogadorDaRodada;

  public JogoPedraMaior(){
    this.monte = new ArrayList<Pedra>();
    this.jogadorA = new Jogador();
    this.jogadorA.nome = "A";

    this.jogadorB = new Jogador();
    this.jogadorB.nome = "B";
  }

  public void iniciar(){
      this.criarPedras();
      this.fazerJogadorComprarPedrasAleatoriamente(this.jogadorA,3);
      this.fazerJogadorComprarPedrasAleatoriamente(this.jogadorB,3);

      primeiroJogadorDaRodada = this.jogadorA;
      segundoJogadorDaRodada = this.jogadorB;
  }

  public void rodada(){

    this.fazerJogadorComprarPedrasAleatoriamente(this.jogadorA,1);
    this.fazerJogadorComprarPedrasAleatoriamente(this.jogadorB,1);

    Pedra p1 = this.primeiroJogadorDaRodada.jogarPedra();
    Pedra p2 = this.segundoJogadorDaRodada.jogarPedra();

    System.out.println(this.primeiroJogadorDaRodada.nome+" jogou "+p1.valor);
    System.out.println(this.segundoJogadorDaRodada.nome+" jogou "+p2.valor);

    Pedra ganhadora = maiorPedra(p1, p2);

    if(ganhadora != null){
      System.out.println("ganhou pedra com valor "+ganhadora.valor);
    }else{
      System.out.println("empate!");
    }

    if(ganhadora == p1){
      this.primeiroJogadorDaRodada.pontos++;
      this.inverterQuemComeca();
    }
    else if(ganhadora == p2){
      this.segundoJogadorDaRodada.pontos++;
    }
    else{
      this.monte.add(p1);
      this.monte.add(p2);
    }
  }

  public void inverterQuemComeca(){
    if(this.primeiroJogadorDaRodada == this.jogadorA){
      this.primeiroJogadorDaRodada = this.jogadorB;
      this.segundoJogadorDaRodada = this.jogadorA;
    }else{
      this.primeiroJogadorDaRodada = this.jogadorA;
      this.segundoJogadorDaRodada = this.jogadorB;
    }
  }

  public void criarPedras(){
    for(int i = 0;i<10;i++){
      for(int j = 1;j<4;j++){
        Pedra p = new Pedra();
        p.valor = j;
        this.monte.add(p);
      }
    }
  }

  public void fazerJogadorComprarPedrasAleatoriamente(Jogador jogador, int quantidade){
    for(int i  = 0;i<quantidade;i++){
      Random ran = new Random();
      int indiceSorteado = ran.nextInt(this.monte.size());
      Pedra pedraSorteada = this.monte.remove(indiceSorteado);
      jogador.comprarPedra(pedraSorteada);
    }
  }

  public Pedra maiorPedra(Pedra p1, Pedra p2){
    if(p1.valor > p2.valor){
      return p1;
    }
    else if(p1.valor < p2.valor){
      return p2;
    }
    else{
      return null;
    }
  }
}

class Jogador{
  ArrayList<Pedra> mao;
  int pontos;
  String nome;

  public Jogador(){
    this.pontos = 0;
    this.mao = new ArrayList<Pedra>();
  }

  public Pedra jogarPedra(){
    Random ran = new Random();
    int indiceSorteado = ran.nextInt(this.mao.size());
    return this.mao.remove(indiceSorteado);
  }

  public void comprarPedra(Pedra p){
    this.mao.add(p);
  }
}
